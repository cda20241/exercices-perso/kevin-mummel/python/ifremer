.. Ifremer documentation master file, created by
   sphinx-quickstart on Tue Nov  7 15:58:15 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Ifremer's documentation!
===================================

.. toctree::
   :maxdepth: -1
   :caption: Contents:


./modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
