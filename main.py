import matrice  # Importez le module "matrice"
import fonctions  # Importez le module "fonctions"

""" Charger et remplir les matrices à partir du fichier CSV """
matrices_par_date = matrice.matrice_par_date('marees_v1.csv')

""" Présentation"""
print("---------- Hello world, welcome to the Ifremer project! ----------")

""" Donne le point le plus haut de la plage qui est immergé """
fonctions.point_plus_haut(matrice.matrices_par_date)

""" Donne les coordonnées des points toujours immergés """
fonctions.valeurs_toujours_egal_1(matrice.matrices_par_date)

""" Donne les coordonnées des points jamais immergés """
fonctions.valeurs_toujours_egal_0(matrice.matrices_par_date)

""" Donne le pourcentage de temps sous l'eau d'un capteur choisi dans un laps de temps choisi. """
fonctions.pourcentage_point_sous_leau(matrice.matrices_par_date)

""" Donne la date du plus gros coefficient de marée """
fonctions.date_avec_plus_grand_nombre_de_1(matrice.matrices_par_date)

""" Donne la date du plus petit coefficient de marée """
fonctions.date_avec_plus_proche_nombre_de_1_et_0(matrice.matrices_par_date)
