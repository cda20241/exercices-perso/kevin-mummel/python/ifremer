from datetime import datetime  # Importer le module "datetime" présent d'origine sur python


def point_plus_haut(matrices_par_date):
    """ Détecter et afficher le point le plus haut immergés en vérifiant si un capteur est actif
        en coordonnées y=2, puis y=1 puis y=0 """
    point_le_plus_haut = None

    for date, matrice in matrices_par_date.items():
        for y_range in range(3, 0, -1):
            for y in range(y_range):
                for x, valeur in enumerate(matrice[y]):
                    if valeur == 1:
                        if point_le_plus_haut is None or y > point_le_plus_haut[2]:
                            point_le_plus_haut = (date, x, y)

    if point_le_plus_haut:
        date, x, y = point_le_plus_haut
        print(f'La marée atteint le point le plus haut à la date du: {date}, '
              f'aux coordonnées (x={x} et y={y})')
    else:
        print("Aucun point le plus haut n'a été trouvé.")


def valeurs_toujours_egal_1(matrices_par_date):
    """ Comparer si il y a des occurences de valeur 1 dans les matrices triées par date.
        Si il y a le même nombre d'occurences que de nombre de matrices alors on affiche les coordonnées """
    # Initialisez un dictionnaire pour suivre le nombre d'occurrences de 1 pour chaque coordonnée (x, y)
    occurences_1 = {}

    # Parcourez les matrices 3x3 par date
    for date, matrice in matrices_par_date.items():
        for y in range(3):
            for x in range(3):
                valeur = matrice[y][x]
                if valeur == 1:
                    # Incrémente le nombre d'occurrences de 1 pour cette coordonnée
                    occurences_1[(x, y)] = occurences_1.get((x, y), 0) + 1

    # Parcourez le dictionnaire d'occurrences 1 pour trouver les coordonnées qui ont une
    # occurrence de 1 pour chaque date
    for coord, occ in occurences_1.items():
        if occ == len(matrices_par_date):
            x, y = coord
            print(f' - Le capteur est toujours immergé aux Coordonnées (x={x} et y={y})')


def valeurs_toujours_egal_0(matrices_par_date):
    """ Comparer si il y a des occurences de valeur 0 dans les matrices triées par date.
        Si il y a le même nombre d'occurences que de nombre de matrices alors on affiche les coordonnées """
    # Initialisez un dictionnaire pour suivre le nombre d'occurrences de 1 pour chaque coordonnée (x, y)
    occurences_1 = {}

    # Parcourez les matrices 3x3 par date
    for date, matrice in matrices_par_date.items():
        for y in range(3):
            for x in range(3):
                valeur = matrice[y][x]
                if valeur == 0:
                    # Incrémente le nombre d'occurrences de 1 pour cette coordonnée
                    occurences_1[(x, y)] = occurences_1.get((x, y), 0) + 1

    # Parcourez le dictionnaire d'occurrences 1 pour trouver les coordonnées qui ont une
    # occurrence de 1 pour chaque date
    for coord, occ in occurences_1.items():
        if occ == len(matrices_par_date):
            x, y = coord
            print(" - Le capteur n'est jamais immergé aux Coordonnées" f' (x={x} et y={y})')


def pourcentage_point_sous_leau(matrices_par_date):
    """ Determiner le pourcentage de temps d'un capteur sous l'eau d'après une plage de temps et
        de coordonnées fournis par l'utilisateur """
    choix_date_debut = input("Choisissez une date de début entre 2022-01-01-02 et 2022-01-02-12: ")
    choix_date_fin = input("Choisissez une date de fin entre 2022-01-01-02 et 2022-01-02-12: ")
    choix_x = int(input("Choisissez les coordonnées x entre 0 et 2 d'un point sur la plage: "))
    choix_y = int(input("Choisissez les coordonnées y entre 0 et 2 d'un point sur la plage: "))

    # Vérifiez si les coordonnées x et y sont valides
    if not (0 <= choix_x <= 2) or not (0 <= choix_y <= 2):
        print("Erreur : x et y doivent être compris entre 0 et 2")
        return

    # Convertissez les dates de début et de fin en objets datetime
    date_debut = datetime.strptime(choix_date_debut, "%Y-%m-%d-%H")
    date_fin = datetime.strptime(choix_date_fin, "%Y-%m-%d-%H")

    # Initialisez le compteur de présence du point sous l'eau
    capteur_sous_eau = 0

    # Initialisez le compteur de dates entre la date de début et la date de fin
    nombre_dates_entre_dates = 0

    # Parcourez les dates dans la plage spécifiée
    for date, matrice in matrices_par_date.items():
        date_format = datetime.strptime(date, "%Y-%m-%d-%H")

        if date_debut <= date_format <= date_fin:
            nombre_dates_entre_dates += 1

            if matrice[choix_x][choix_y] == 1:
                capteur_sous_eau += 1

    if nombre_dates_entre_dates > 0:
        pourcentage = (capteur_sous_eau / nombre_dates_entre_dates) * 100
        print(f"Le capteur aux coordonnées (x={choix_x}, y={choix_y}) est sous l'eau {pourcentage:.2f}% du temps "
              f"entre le {choix_date_debut} et le {choix_date_fin}.")
    else:
        print("Aucune date trouvée entre la date de début et la date de fin. ")


def date_avec_plus_grand_nombre_de_1(matrices_par_date):
    """ Determiner à quelle date il y a le plus grand nombre d'occurence de capteur à la valeur 1
        ce qui correspondra à la plus grande marée """
    date_max_1 = None
    max_nombre_1 = 0

    for date, matrice in matrices_par_date.items():
        nombre_1 = sum(sum(row) for row in matrice)
        if nombre_1 > max_nombre_1:
            max_nombre_1 = nombre_1
            date_max_1 = date

    if date_max_1 is not None:
        print(
            f"Le plus gros coefficient de marée est le {date_max_1} avec {max_nombre_1} "
            f"occurrences de capteurs immergés.")
    else:
        print("Aucune date trouvée avec des 1.")


def date_avec_plus_proche_nombre_de_1_et_0(matrices_par_date):
    """ Donner le jour où le nombres de capteurs actif est le plus proches du nombre de capteur inactif.
        Ce qui correspondra au minimum d'amplitude de la marrée donc au plus petit coefficient. """
    dates_max_proches = []  # Initialisez une liste pour stocker les dates les plus proches
    diff_min = float('inf')  # Initialise la différence minimale avec une valeur infinie pour commencer
    # la comparaison avec la plus grosse valeur possible

    for date, matrice in matrices_par_date.items():
        nombre_1 = sum(sum(row) for row in matrice)
        nombre_0 = sum(sum(1 for cell in row if cell == 0) for row in matrice)

        # Calcule la différence absolue entre le nombre de 1 et de 0
        diff = abs(nombre_1 - nombre_0)

        # Si la différence actuelle est inférieure ou égale à la différence minimale, met à jour la liste des dates
        if diff <= diff_min:
            # Si la différence actuelle est plus petite, efface la liste et ajoute la date
            if diff < diff_min:
                dates_max_proches = []
                diff_min = diff
            dates_max_proches.append(date)

    if dates_max_proches:
        print("Les dates du plus petit coefficient de marée sont les suivantes :")
        for date in dates_max_proches:
            print(f"{date} (Différence entre capteurs actif et inactif = {diff_min})")
    else:
        print("Aucune date trouvée avec des données.")
